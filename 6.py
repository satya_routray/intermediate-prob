"""Write a program that automatically converts English text to Morse code and vice versa."""

dictionary = {'A': '.-', 'B': '-...',   'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....', 'I': '..',
        'J': '.---',  'K': '-.-', 'L': '.-..',  'M': '--',     'N': '-.', 'O': '---','P': '.--.',   'Q': '--.-',   'R': '.-.',
     	'S': '...', 'T': '-', 'U': '..-', 'V': '...-',  'W': '.--',   'X': '-..-',
        'Y': '-.--',   'Z': '--..',  " " : " "}
    
def convert(msg):
	
    result = " "
        
    for i in msg.upper():
        result = result + dictionary.get(i) 

    print result 
    	
if __name__ == "__main__":
	convert("i am a guy")
